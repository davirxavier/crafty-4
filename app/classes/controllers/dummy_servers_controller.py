import asyncio
import logging
from typing import Dict, Callable

from twisted.internet.defer import Deferred
from twisted.internet.tcp import Port

from app.classes.shared.dummy_servers import DummyServers, DummyServerFactory
from app.classes.shared.singleton import Singleton

logger = logging.getLogger(__name__)


class DummyServersController(metaclass=Singleton):
    """Controller for dummy servers."""

    def __init__(self):
        self.dummy_servers = DummyServers()
        self.dummy_by_id: Dict[str, Port] = {}

    def create_dummy_server(
        self,
        server_id: str,
        server_port: int,
        hibernation_motd: str,
        wake_up_message: str,
        online_mode: bool = True,
        max_players: int = 1,
        player_connected_callback: Callable = None,
    ):
        """Starts a new dummy server with the defined configuration.
        Parameters:
            server_id: The real server id.
            server_port: The port to bind the dummy server.
            hibernation_motd: The server description in the multiplayer menu.
            wake_up_message: The message shown to the player when they connect.
            online_mode: Enable online mode for the dummy server.
            max_players: The max players allowed.
            player_connected_callback: The callback for when a player connects to the server. Will be called
                immediately after the "Connecting to server..." message appears to a player that is joining.
        """

        async def do_create():
            factory = DummyServerFactory()

            factory.motd = hibernation_motd
            factory.close_message = wake_up_message
            factory.max_players = max_players
            factory.online_mode = online_mode
            factory.player_connected_callback = player_connected_callback

            self.dummy_by_id[server_id] = self.dummy_servers.reactor.listenTCP(
                server_port, factory, interface="0.0.0.0"
            )
            logger.info(f"Created dummy server in port {server_port}.")

        asyncio.run_coroutine_threadsafe(do_create(), self.dummy_servers.loop)

    def stop_dummy_server(self, server_id, port_freed_callback: Callable = None):
        """Stops a dummy server for the server_id specified and frees up the used port.
        Parameters:
            server_id: The real server id.
            port_freed_callback: The function that will be called when the port used by the dummy server
                is completely freed.
        """

        async def do_stop():
            port: Port = self.dummy_by_id.pop(server_id, None)
            if port:
                deferred: Deferred = port.loseConnection()
                if deferred and port_freed_callback:
                    deferred.addCallback(lambda ret: port_freed_callback())
                    deferred.addErrback(
                        lambda err: logger.error(
                            "Error while trying to stop dummy server: ", err
                        )
                    )
                elif port_freed_callback:
                    port_freed_callback()
            elif port_freed_callback:
                port_freed_callback()

        asyncio.run_coroutine_threadsafe(do_stop(), self.dummy_servers.loop)

    def is_dummy_server_running(self, server_id):
        return server_id in self.dummy_by_id
