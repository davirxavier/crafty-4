import logging
import sys
import threading
import asyncio
from typing import Callable

import quarry.data.packets as packets
from quarry.net.server import ServerFactory, ServerProtocol
from twisted.internet import asyncioreactor

from app.classes.shared.singleton import Singleton

logger = logging.getLogger(__name__)


class DummyServers(metaclass=Singleton):
    """Manager for the dummy server thread"""

    def __init__(self):
        self.thread = threading.Thread(
            daemon=True,
            target=self.run_thread,
            name="dummy_servers_thread",
        )
        self.reactor: asyncioreactor.AsyncioSelectorReactor | None = None
        self.loop: asyncio.AbstractEventLoop | None = None
        self.init_quarry_protocols()

    def init_servers(self):
        self.thread.start()

    def run_thread(self):
        if "twisted.internet.reactor" in sys.modules:
            del sys.modules["twisted.internet.reactor"]

        self.loop = asyncio.new_event_loop()
        asyncioreactor.install(self.loop)

        from twisted.internet import reactor

        self.reactor = reactor
        self.reactor.run(installSignalHandlers=False)

    def init_quarry_protocols(self):
        new_versions = [(761, '1.19.3'), (762, '1.19.4'), (763, '1.20'), (764, '1.20.2'), (765, '1.20.3')]

        for v in new_versions:
            packets.minecraft_versions[v[0]] = v[1]

        max_version = packets.default_protocol_version
        last_version_names = [(k, v) for k, v in packets.packet_names.items() if k[0] == max_version]
        for packet_info in last_version_names:
            key = packet_info[0]
            name = packet_info[1]

            for version in new_versions:
                packets.packet_names[(version[0], key[1], key[2], key[3])] = name

        last_version_idents = [(k, v) for k, v in packets.packet_idents.items() if k[0] == max_version]
        for ident in last_version_idents:
            key = ident[0]
            val = ident[1]

            for version in new_versions:
                packets.packet_idents[(version[0], key[1], key[2], key[3])] = val


class DummyServerProtocol(ServerProtocol):
    """Class that implements what to do after client connects to dummy server."""

    def player_joined(self):
        logger.info("Player connected in server. Stopping dummy server.")
        # ServerProtocol.player_joined(self)
        self.close(self.factory.close_message)

        if self.factory.player_connected_callback:
            self.factory.player_connected_callback()


class DummyServerFactory(ServerFactory):
    """Factory class for a dummy server."""

    def __init__(
        self,
        motd: str = "",
        close_message: str = "",
        online_mode: bool = False,
        max_players: int = 20,
        player_connected_callback: Callable = None,
    ):
        super().__init__()
        self.protocol = DummyServerProtocol
        self.motd = motd
        self.close_message = close_message
        self.online_mode = online_mode
        self.max_players = max_players
        self.player_connected_callback = player_connected_callback
